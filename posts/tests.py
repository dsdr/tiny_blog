from django.test import TestCase	#using django test framework
from django.core.exceptions import ObjectDoesNotExist #exception for TestPosts class
from posts.models import Post	#include model to test

#Hi, I'm Pino, I'll test functional side of project by nose framework

#Pino tries to create new post object without interface
class TestPostModel(TestCase):
    #def setUp(self):
    def tearDown(self):
        pass
    #check, if post object in model exists
    def test_exist(self):
    	try:
    		p = Post()
    	except ObjectDoesNotExist:
    		assert False
