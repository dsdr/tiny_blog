from django.test import LiveServerTestCase
from collections import namedtuple  #to keep settings
from selenium import webdriver  #for func testing
from selenium.webdriver.common.keys import Keys #for input 

from views import * #import page to test


#there Foxy keeps her settings&properties
Settings = namedtuple('Settings', ['login', 'password'])
settings_admin_form = Settings(
    login="admin",
    password="god",
)

class TestAdmin(LiveServerTestCase):
    fixtures = ['admin_user.json']

    def setUp(self):
    	#Hello, I'm Foxy, and I'll test this blog!
    	self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        #Be good girl, don't forget to close the door
        self.browser.quit()

    def test_admin_page_create_blog_page(self):
    	#Foxy try to open admin page in browser
    	self.browser.get(self.live_server_url + '/admin/')

    	#Foxy try to find login form
    	body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Django administration', body.text)

        #and login from 
        username_field = self.browser.find_element_by_name('username')
        username_field.send_keys(settings_admin_form.login)
        password_field = self.browser.find_element_by_name('password')
        password_field.send_keys(settings_admin_form.password)
        password_field.send_keys(Keys.RETURN)

        #that, checks, if login successfully
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Site administration', body.text)


        #Okay, we are on admin page.
        #Let Foxy try to create new blog post
        self.browser.find_elements_by_link_text('Posts')[1].click()
        self.browser.find_element_by_link_text('Add post').click()

